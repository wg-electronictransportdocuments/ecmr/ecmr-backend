/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.persistence.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.Instant;

@Entity
@Table(name = "SEALED_DOCUMENT")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SealedDocumentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @CreationTimestamp
    private Instant created;
    @UpdateTimestamp
    private Instant last_updated;
    @Version
    private Integer version;
    @Lob
    private String seal;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "sealed_ecmr_id")
    private SealedEcmrEntity sealedEcmr;
}




