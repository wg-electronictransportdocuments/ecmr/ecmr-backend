/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.openlogisticsfoundation.ecmr.domain.exceptions;

public class TemplateUserNotFoundException extends Exception {
    public TemplateUserNotFoundException(Long id) {
        super("TemplateUser with id " + id + " not found");
    }
}
